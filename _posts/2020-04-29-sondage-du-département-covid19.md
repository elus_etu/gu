---
title: Sondage du département sur le confinement
layout: post
date: 2020-04-29 21:20
image: /assets/images/posts/questionnaire.png  
headerImage: true
category: actualites
author: elusgu
tag: événement
description: Le département GU a besoin de votre avis !
---

Le département GU a besoin de votre avis !

Comment vivez-vous le confinement ? Avez-vous des éléments à remonter sur votre situation ?

[Répondez via ce formulaire!](https://docs.google.com/forms/d/e/1FAIpQLSeP_2bp9mYL6LPo3bN-00Vzomb3fSNN4OfBw56rvFEZ8hhJjg/viewform)
