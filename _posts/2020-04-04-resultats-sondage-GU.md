---
title: Résultats du sondage sur le département
layout: post
date: 2020-04-04 12:00
image: /assets/images/posts/questionnaire.png  
headerImage: true
category: actualites
author: elusgu
tag: événement
description: Stage, UV, perception de la branche... on vous dit tout !
---
Du 11 mars au 29 mars 2020 nous avons mené une enquête auprès de 60 étudiant.e.s de GU sur toutes les thématiques de notre branche : stages, UV, nouvelles filières, perception de la branche, recherche... Vous pouvez retrouver [les résultats ici](https://cloud.elus-etu.utc.fr/index.php/s/MASN4rjXZQfoqte#pdfviewer) !

Ceux-ci ont été communiqués aux enseignants et à l'administration GU. 

Merci à tou.te.s pour votre participation, nous allons dans un premier temps essayer de promouvoir notre branche auprès des TC (notamment avec ce site internet), vous pourrez retrouver nos autres pistes d'actions à la fin du document. 

_Bonne lecture !_
