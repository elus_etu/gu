---
title: Les soutenances de stage A19
layout: post
date: 2020-02-26 12:00
image: /assets/images/posts/internship.png  
headerImage: true
category: actualites
author: elusgu
tag: événement
description: Planning des soutenances, prix poster...
---
Les soutenances de stage sont un moment sympa dans la vie de la branche : affichage des posters, prix du meilleur poster, déjeuner le midi et visites des laboratoires... Une chouette journée pour se retrouver entre profs et étudiant.e.s.

Vous pourrez retrouver [le planning de passsage (avec les noms d'entreprises) ici](https://cloud.elus-etu.utc.fr/index.php/s/aBDJjGKzM9rqSyX) (pratique si vous cherchez un stage et avez besoin d'inspiration).  

Et suspens... retrouvez les photos des 5 posters finalistes _ci-dessous_ !
![]({{site.url}}/assets/images/posts/poster1.jpg)
![]({{site.url}}/assets/images/posts/poster2.jpg)
![]({{site.url}}/assets/images/posts/poster3.jpg)
![]({{site.url}}/assets/images/posts/poster4.jpg)
![]({{site.url}}/assets/images/posts/poster5.jpg)
